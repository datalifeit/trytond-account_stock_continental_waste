# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool, PoolMeta
from trytond.model import ModelSQL, fields
from trytond.pyson import Eval
from trytond.modules.account_product.product import account_used
from trytond.modules.company.model import (
    CompanyMultiValueMixin, CompanyValueMixin)


ACCOUNT_STOCK_DOMAIN = [
    ('closed', '!=', True),
    ('type.stock', '=', True),
    ('type.statement', '=', 'income'),
]


class Location(CompanyMultiValueMixin, metaclass=PoolMeta):
    __name__ = 'stock.location'

    waste = fields.Boolean('Waste',
        states={'invisible': (Eval('type') != 'lost_found')},
        depends=['type'])
    account_stock_in = fields.MultiValue(fields.Many2One(
            'account.account', "Account Stock IN",
            domain=ACCOUNT_STOCK_DOMAIN + [
                ('company', '=', Eval('context', {}).get('company', -1))],
            states={
                'invisible': (Eval('type') != 'lost_found'),
            },
            depends=['type']))
    account_stock_out = fields.MultiValue(fields.Many2One(
            'account.account', "Account Stock OUT",
            domain=ACCOUNT_STOCK_DOMAIN + [
                ('company', '=', Eval('context', {}).get('company', -1))],
            states={
                'invisible': (Eval('type') != 'lost_found'),
            },
            depends=['type']))
    account_stocks = fields.One2Many('stock.location.account', 'location',
        'Accounts Stock')

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field in {'account_stock_in', 'account_stock_out'}:
            return pool.get('stock.location.account')
        return super().multivalue_model(field)

    @property
    @account_used('account_stock_in')
    def account_stock_in_used(self):
        pass

    @property
    @account_used('account_stock_out')
    def account_stock_out_used(self):
        pass

    def get_account(self, name, **pattern):
        return self.get_multivalue(name[:-5], **pattern)


class LocationAccount(ModelSQL, CompanyValueMixin):
    '''Stock Location - Account'''
    __name__ = 'stock.location.account'

    location = fields.Many2One('stock.location', 'Location', required=True)
    account_stock_in = fields.Many2One('account.account', "Account Stock IN",
        domain=ACCOUNT_STOCK_DOMAIN + [('company', '=', Eval('company', -1))],
        depends=['company'])
    account_stock_out = fields.Many2One('account.account', "Account Stock OUT",
        domain=ACCOUNT_STOCK_DOMAIN + [('company', '=', Eval('company', -1))],
        depends=['company'])
