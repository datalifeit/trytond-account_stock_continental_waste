========================================
Account Stock Continental Waste Scenario
========================================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts
    >>> from trytond.modules.account_stock_continental.tests.tools import \
    ...     add_stock_accounts
    >>> from decimal import Decimal
    >>> import datetime

Install account_stock_continental_waste::

    >>> config = activate_modules('account_stock_continental_waste')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = create_fiscalyear(company)
    >>> fiscalyear.account_stock_method = 'continental'
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = add_stock_accounts(get_accounts(company), company)
    >>> receivable = accounts['receivable']
    >>> payable = accounts['payable']
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']
    >>> stock = accounts['stock']
    >>> stock_in = accounts['stock_expense']
    >>> stock_out, = stock_in.duplicate()
    >>> stock_in2, = stock_in.duplicate()

Create parties::

    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier')
    >>> supplier.save()

Create product category::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.account_stock = stock
    >>> account_category.account_stock_in = stock_in
    >>> account_category.account_stock_out = stock_out
    >>> account_category.save()

Create product::

    >>> Uom = Model.get('product.uom')
    >>> Template = Model.get('product.template')

    >>> unit, = Uom.find([('name', '=', 'Unit')])
    >>> template = Template()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category
    >>> template.save()
    >>> product, = template.products
    >>> product.cost_price = Decimal('6')
    >>> product.save()

Receive products::

    >>> ShipmentIn = Model.get('stock.shipment.in')
    >>> Move = Model.get('stock.move')
    >>> shipment = ShipmentIn(supplier=supplier)
    >>> move = shipment.incoming_moves.new()
    >>> move.product = product
    >>> move.uom = unit
    >>> move.quantity = 1
    >>> move.unit_price = Decimal('5')
    >>> move.from_location = supplier.supplier_location
    >>> move.to_location = shipment.warehouse.input_location
    >>> move = shipment.incoming_moves.new()
    >>> move.product = product
    >>> move.uom = unit
    >>> move.quantity = 4
    >>> move.unit_price = Decimal('5')
    >>> move.from_location = supplier.supplier_location
    >>> move.to_location = shipment.warehouse.input_location
    >>> shipment.save()
    >>> shipment.click('receive')
    >>> shipment.click('done')

Check moves::

    >>> AccountMoveLine = Model.get('account.move.line')
    >>> lines = AccountMoveLine.find([('account', '=', stock)])
    >>> len(lines)
    2

Create location account stock::

    >>> Location = Model.get('stock.location')
    >>> location, = Location.find([('name', '=', 'Lost and Found')])
    >>> location2, = location.duplicate()
    >>> location2.account_stock_in = stock_in2
    >>> location2.account_stock_out = stock_in2
    >>> location2.save()

Get storage::

    >>> Inventory = Model.get('stock.inventory')
    >>> storage, = Location.find([
    ...         ('code', '=', 'STO'),
    ...         ])

Try confirm an Inventory with Lost Found without Stock Accounts::

    >>> inventory = Inventory()
    >>> inventory.location = storage
    >>> inventory.click('complete_lines')
    >>> inventory_line, = inventory.lines
    >>> inventory_line.quantity = 2.0
    >>> inventory.click('confirm')  # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    trytond.modules.account_product.exceptions.AccountError: ...

Set lost and found location::

    >>> warehouse = storage.warehouse
    >>> warehouse.lost_found_location = location2
    >>> warehouse.save()

Create a Inventory with custom account::

    >>> inventory = Inventory()
    >>> inventory.location = storage
    >>> inventory.click('complete_lines')
    >>> inventory_line, = inventory.lines
    >>> inventory_line.quantity = 0.0
    >>> inventory.click('confirm')
    >>> lines = AccountMoveLine.find([('account', '=', stock_in2)])
    >>> len(lines)
    1